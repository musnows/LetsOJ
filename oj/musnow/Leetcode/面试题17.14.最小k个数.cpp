class Solution {
public:
    vector<int> smallestK(vector<int>& arr, int k) {
        priority_queue<int,vector<int>,greater<int>> que;
        for(auto&i:arr){
            que.push(i);
        }
        vector<int> retV;
        while(k-- && !que.empty())
        {
            retV.push_back(que.top());
            que.pop();
        }
        return retV;
    }
};

// https://leetcode.cn/problems/smallest-k-lcci/
// 设计一个算法，找出数组中最小的k个数。以任意顺序返回这k个数均可。

// 示例：

// 输入： arr = [1,3,5,7,2,4,6,8], k = 4
// 输出： [1,2,3,4]

// 这道题比较简单，直接用小堆就行了。