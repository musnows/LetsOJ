class Solution {
public:
    vector<int> nextGreaterElements(vector<int>& nums) {
        // 这到题和739完全一样，只不过是循环数组；
        // 因为结果不需要我们写距离，而是记录元素，那么问题就很好解决了；
        // 我们可以把原本数组拼接一次，然后直接进行判断，再把判断结果resize为原数组大小即可。
        // vector<int> nums1(nums.begin(), nums.end());
        // nums1.insert(nums1.end(), nums.begin(), nums.end());
        // 但是这样会有额外的扩容拼接的消耗，我们还不如直接用for循环模拟两次遍历nums；
        stack<int> st;
        st.push(0); // 下标
        // 结果集
        vector<int> retV(nums.size(), -1);
        // 从1开始遍历
        for (int k = 1; k < nums.size() * 2; k++) {
            int i = k % nums.size(); // 实际的下标
            if (nums[i] <= nums[st.top()]) {
                st.push(i);
            } else {
                while (!st.empty() && nums[i] > nums[st.top()]) {
                    retV[st.top()] = nums[i];
                    st.pop();
                }
                st.push(i);
            }
        }
        return retV;
    }
};
