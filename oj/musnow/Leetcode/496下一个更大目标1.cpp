class Solution {
public:
    vector<int> nextGreaterElement(vector<int>& nums1, vector<int>& nums2) {
        // 题目说1和2数组都没有重复元素，意思是单个数组中没有重复的。
        // 注意，题目要求的是1中的元素，在2里面对应的下一个更大元素是谁
        // 本质上是让我们求2中的某一个元素的下一个更大元素是谁，这里的“某一个元素”被nums1固定了。
        // 所以结果的数量是nums1。
        vector<int> retV(nums1.size(), -1);
        // 使用map来映射1中的元素和1中的下标，方便设置结果
        unordered_map<int, size_t> index1Map;
        for (size_t i = 0; i < nums1.size(); i++) {
            index1Map[nums1[i]] = i;
        }
        // nums1只是一个给出的要找下一个更大元素的集合，真的要找的东西都在2里面，得遍历2。
        stack<int> st; // 同样是从栈顶到栈底单调递增的栈，存放2中的下标。
        st.push(0); // 第一个元素直接入栈
        for (size_t i = 1; i < nums2.size(); i++) {
            // 因为求的是下一个比当前元素大的，所以小于等于的都直接入栈
            if (nums2[i] <= nums2[st.top()]) {
                st.push(i);
            } else {
                // while循环一直判断是否比当前元素小
                while (!st.empty() && nums2[i] > nums2[st.top()]) {
                    // 判断这个栈顶元素是否被nums1选中需要处理
                    auto ret = index1Map.find(nums2[st.top()]);
                    if (ret != index1Map.end()) {
                        // 取下标
                        size_t indexIn1 = ret->second;
                        // 设置下一个更大的元素是谁（不是设置距离了）
                        retV[indexIn1] = nums2[i];
                    }
                    // 不管有没有被选中都须出栈
                    st.pop();
                }
                // 当前元素入栈
                st.push(i);
            }
        }
        return retV;
    }
};
