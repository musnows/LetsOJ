class Solution {
public:
    int trap(vector<int>& height) {
        stack<int> st;
        // 栈里面存下标，栈顶到栈底单调递增
        st.push(0);
        int sum = 0;
        for (size_t i = 1; i < height.size(); i++) {
            auto& cur = height[i];
            // 1.direct push to stack
            if (cur < height[st.top()]) {
                st.push(i);
            }
            // 2.replace index of top
            else if (cur == height[st.top()]) {
                st.pop();
                st.push(i);
            }
            // 3.clac space of rain
            else {
                while (!st.empty() && cur > height[st.top()]) {
                    int bottomIndex = st.top();
                    st.pop();
                    // 注意刚开始的时候可能只有一个元素
                    if (!st.empty()) {
                        int leftIndex = st.top();
                        // 错误！左边的元素不能被pop掉，因为它可能是下一个凹槽要用的底部值！
                        // st.pop();
                        // 计算凹槽高度和宽度
                        int h =
                            min(height[leftIndex], cur) - height[bottomIndex];
                        int w = i - leftIndex - 1;
                        // 雨水体积
                        sum += h * w;
                    }
                }
                st.push(i);
            }
        }
        return sum;
    }
};
